import React, { Component } from "react";
import { NavLink } from "react-router-dom";

class Header extends Component {
  active = {
    backgroundColor: "black",
    color: "white",
    fondWeight: "bold"
  };

  header = {
    listStyle: "none",
    display: "flex",
    justifyContent: "space-evenly"
  };

  render() {
    return (
      <div style={this.header}>
        <NavLink exact to="/" activeStyle={this.active}>
          Home
        </NavLink>
        <NavLink to="/MyIdeas" activeStyle={this.active}>
          My Ideas
        </NavLink>
      </div>
    );
  }
}

export default Header;
