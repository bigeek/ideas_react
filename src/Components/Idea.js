import React, { Component } from "react";
import Save from "@material-ui/icons/Save";
import Edit from "@material-ui/icons/Edit";
import Delete from "@material-ui/icons/Delete";

class Idea extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editing: false
    };

    this.renderUI = this.renderUI.bind(this);
    this.renderForm = this.renderForm.bind(this);
    this.edit = this.edit.bind(this);
    this.delete = this.delete.bind(this);
    this.save = this.save.bind(this);
  }

  save(e) {
    e.preventDefault();

    //this.props.onChange(this.newIdea.value, this.props.index);

    this.setState({
      editing: false
    });
  }

  edit() {
    this.setState({
      editing: true
    });
  }

  delete() {
    this.props.onDelete(this.props.index);
  }

  renderForm() {
    return (
      <div>
        <form>
          <textarea ref={input => (this.newIdea = input)} />
          <button onClick={this.save}>
            <Save />
            Save
          </button>
        </form>
      </div>
    );
  }

  renderUI() {
    return (
      <div className="idea">
        <div>{this.props.children}</div>
        <span>
          <button onClick={this.edit}>
            <Edit />
            Edit
          </button>
          <button onClick={this.delete}>
            <Delete />
            Delete
          </button>
        </span>
      </div>
    );
  }

  render() {
    return this.state.editing ? this.renderForm() : this.renderUI();
  }
}

export default Idea;
