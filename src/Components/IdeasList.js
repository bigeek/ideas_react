import React, { Component } from "react";
import Idea from "./Idea";

import Add from "@material-ui/icons/Add";

import data from './../Data/data.json'

class IdeasList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ideas: [
        //{ id: 1, idea: "Tripper API", group: "Tamar, Haim" },
        //{ id: 7, idea: "Cyber one", group: "Dan, Eden" },
        //{ id: 9, idea: "Intellmep", group: "Dima, Daria" }
      ]
    };

    this.eachIdea = this.eachIdea.bind(this);
    this.update = this.update.bind(this);
    this.delete = this.delete.bind(this);
    this.add = this.add.bind(this);
    this.nextId = this.nextId.bind(this);
  }

  componentDidMount(){
    data.map(item => {
      console.log("idea: " + item.idea);
      this.add({id: item.id, txt: item.idea, grp: item.group});
    })
  }

  add({event = null, id = null, txt = 'default title', grp = 'default group'}){
    console.log(event, id, txt, grp);

    this.setState(prevState => ({
      ideas: [
        ...prevState.ideas, {
          id: id !== null ? id : this.nextId(prevState.ideas),
          idea: txt,
          group:grp
        }
      ]
    }))
  }

  nextId(ideas = []){
    let max = ideas.reduce((prev, curr) => prev.id > curr.id ? prev.id : curr.id , 0)
    return ++max;
  }

  update(newIdea, i){
    console.log(`Update ${i}: ${newIdea}`);

    this.setState(prevState => ({
      ideas: prevState.ideas.map(
        idea => idea.id !== i ? idea : { ...idea, idea: newIdea}
      )
    }))
  }

  delete(id){
    this.setState(prevState =>({
      ideas: prevState.ideas.filter(idea => idea.id !== id)
    }))
  }

  eachIdea(item, i) {
    return (
      <Idea key={i} index={item.id} onChange={this.update} onDelete={this.delete}>
        <h4>{item.idea}</h4>
        <h5>By: {item.group}</h5>
      </Idea>
    );
  }

  render() {
    return (
      <div className="ideas-list">{this.state.ideas.map(this.eachIdea)}
      <button id="add" onClick={this.add} className="btn btn-primary" style={{marginRight: '7px'}}>
        <Add/>
      </button>
      </div>
    );
  }
}

export default IdeasList;
